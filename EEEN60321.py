# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import numpy
import pyomo.environ as pyo

def get_Ybus(Lines, Slack=0):   
    # Getting number of buses
    No_Buses = 0
    for line in Lines:
        No_Buses = max([No_Buses, line['From'], line['To']])

    # Building Y bus
    M = numpy.zeros((No_Buses-1, No_Buses-1))
    for line in Lines:
        y = 1/line['X']
        i = line['From'] - 1
        j = line['To'] - 1
        
        if i != Slack:
            if i > Slack:
                i -= 1
            M[i, i] += y
            if j != Slack:
                if j > Slack:
                    j -= 1
                M[j, i] -= y
                M[i, j] -= y
                M[j, j] += y
        elif j != Slack:
            if j > Slack:
                j -= 1
            M[j, j] += y

    return M, No_Buses, Slack

def get_Bus_Injections(Generators, Loads, No_Buses, Slack):
    ΔP = numpy.zeros(No_Buses-1)
    for gen in Generators:
        i = gen['Bus']-1
        if i != Slack:
            if i > Slack:
                i -= 1
            ΔP[i] += gen['P']

    for load in Loads:
        i = load['Bus']-1
        if i != Slack:
            if i > Slack:
                i -= 1
            ΔP[i] -= load['Value']

    return ΔP

def solve_DCPF(M, ΔP, No_Buses, Slack):    
    # Solve DC flows
    Δ𝜃 = numpy.linalg.inv(M).dot(ΔP)
    𝜃 = numpy.zeros(No_Buses)
    P = numpy.zeros(No_Buses)
    P[Slack] = -sum(ΔP)
    i = 0
    for x in range(No_Buses):
        if x != Slack:
            𝜃[x] = Δ𝜃[i]
            P[x] = ΔP[i]
            i += 1

    return P, 𝜃

def Visualize_DC(Lines, 𝜃, P):
    No_Buses = len(P)
    print('NET POWER INJECTIONS:')
    for xb in range(No_Buses):
        print('%2.0f) %8.4f MW'%(xb+1, P[xb]))

    print('POWER FLOWS:')
    for line in Lines:
        i = line['From']
        j = line['To']
        f = (𝜃[i-1]-𝜃[j-1])/line['X']
        txt = ''
        if 'Capacity' in line.keys():
            if f > line['Capacity']:
                txt = '(Line constraint violation)'
            elif f == line['Capacity']:
                txt = '(Binding constraint)'
        print('%2.0f-%2.0f) %8.4f %s'%(i, j, f, txt))
    print()

def get_DCPF(Generators, Lines, Loads, Slack=0):
    Y, No_Buses, _ = get_Ybus(Lines, Slack)
    ΔP = get_Bus_Injections(Generators, Loads, No_Buses, Slack)
    P, 𝜃 = solve_DCPF(Y, ΔP, No_Buses, Slack)
    
    return 𝜃, P

def get_Variables(model):
    No_Generators = len(model.Generators)
    No_Lines = len(model.Lines)
    No_Loads = len(model.Loads)

    No_Buses = 0
    for line in model.Lines:
        No_Buses = max([No_Buses, line['From'], line['To']])

    model.Set_Generators = range(No_Generators)
    model.Set_Lines = range(No_Lines)
    model.Set_Loads = range(No_Loads)
    model.Set_Buses = range(No_Buses)

    model.Generation = pyo.Var(model.Set_Generators, domain=pyo.NonNegativeReals)
    model.Power_Flow = pyo.Var(model.Set_Lines, domain=pyo.Reals)
    model.𝜃 = pyo.Var(model.Set_Buses, domain=pyo.Reals)
    model.Losses = pyo.Var(model.Set_Lines, domain=pyo.NonNegativeReals)

def DC_rule(model, xl):
    i = model.Lines[xl]['From']-1
    j = model.Lines[xl]['To']-1
    X = model.Lines[xl]['X']
  
    return model.Power_Flow[xl] == (model.𝜃[i] - model.𝜃[j])/X

def Balance_rule(model, bus):
    G = 0
    i = bus+1
    for xg in model.Set_Generators:
        if model.Generators[xg]['Bus'] == i:
            G = G + model.Generation[xg]

    L = 0
    for xl in model.Set_Loads:
        if model.Loads[xl]['Bus'] == i:
            L = L + model.Loads[xl]['Value']

    P = 0
    for xb in model.Set_Lines:
        if model.Lines[xb]['From'] == i:
            P = P + model.Power_Flow[xb] + model.Losses[xb]
        elif model.Lines[xb]['To'] == i:
            P = P - model.Power_Flow[xb] + model.Losses[xb]

    return G - P == L

def Loss1_rule(model, xl):
    return model.Losses[xl] >= model.Power_Flow[xl]*model.Loss/2
def Loss2_rule(model, xl):
    return model.Losses[xl] >= -1*model.Power_Flow[xl]*model.Loss/2

def Min_Generation_rule(model, xg):
    return model.Generation[xg] >= model.Generators[xg]['Min']

def Max_Generation_rule(model, xg):
    return model.Generation[xg] <= model.Generators[xg]['Max']

def Positive_Line_rule(model, xl):
    return model.Power_Flow[xl] <= model.Lines[xl]['Capacity']

def Negative_Line_rule(model, xl):
    return -model.Power_Flow[xl] <= model.Lines[xl]['Capacity']

def Objective_rule(model):
    Cost = 0
    for xg in model.Set_Generators:
        Cost += model.Generation[xg]*model.Generators[xg]['MC']
    return Cost

def print_OF(model):
    print('Cost = %7.2f £'%model.Objective_Function.expr())

def print_Generation(model):
    print('Generation outputs:')
    for xg in model.Set_Generators:
        G = model.Generation[xg].value
        if G == model.Generators[xg]['Max']:
            txt = '(Binding constraint)'
        else:
            txt = ''
        print('\tG%d     = %7.2f %s'%(xg+1, model.Generation[xg].value, txt))

def print_Power(model):
    print('Power flows:')
    PF = []
    for xl, line in zip(model.Set_Lines, model.Lines):
        i = model.Lines[xl]['From']
        j = model.Lines[xl]['To']
        P = model.Power_Flow[xl].value
        if P > line['Capacity']:
            txt = '(Line constraint violation)'
        elif P == line['Capacity']:
            txt = '(Binding constraint)'
        else:
            txt = ''
        PF.append(P)
        print('\tP%d-%d   = %7.2f %s'%(i, j, P, txt))
    return PF

def print_Nodal(model):
    print('Nodal prices:')
    NP = []
    for xn in model.Set_Buses:
        NP.append(model.dual[model.Constraint_Balance[xn]])
        print('\tMCN%d   = %7.2f'%(xn+1, NP[-1]))
    return NP

def print_MCs(model):
    print('Marginal costs associated with the line capacities:')
    MC = []
    for xl in model.Set_Lines:
        i = model.Lines[xl]['From']
        j = model.Lines[xl]['To']
        if model.dual[model.Constraint_Positive_Line[xl]] == 0:
            if model.dual[model.Constraint_Negative_Line[xl]] == 0:
                MC.append(0)
            else:
                MC.append(-1*model.dual[model.Constraint_Negative_Line[xl]])
        else:
            if model.dual[model.Constraint_Positive_Line[xl]] == 0:
                MC.append(0)
            else:
                MC.append(-1*model.dual[model.Constraint_Positive_Line[xl]])
        print('\tMCL%d-%d = %7.2f'%(i, j, MC[-1]))
    return MC

def print_LP(model):
    print_OF(model)
    print_Generation(model)
    PF = print_Power(model)
    NP = print_Nodal(model)
    MC = print_MCs(model)

def get_LP(Generators, Lines, Loads, Loss=0):
    model = pyo.ConcreteModel()
    model.dual = pyo.Suffix(direction=pyo.Suffix.IMPORT_EXPORT)
    model.Generators = Generators
    model.Lines = Lines
    model.Loads = Loads
    model.Loss = Loss
    get_Variables(model)
    model.Constraint_DC = pyo.Constraint(model.Set_Lines, rule=DC_rule)
    model.Constraint_Balance = pyo.Constraint(model.Set_Buses, rule=Balance_rule)
    model.Constraint_Loss1 = pyo.Constraint(model.Set_Lines, rule=Loss1_rule)
    model.Constraint_Loss2 = pyo.Constraint(model.Set_Lines, rule=Loss2_rule)
    model.Constraint_Min_Generation = pyo.Constraint(model.Set_Generators, rule=Min_Generation_rule)
    model.Constraint_Max_Generation = pyo.Constraint(model.Set_Generators, rule=Max_Generation_rule)
    model.Constraint_Positive_Line = pyo.Constraint(model.Set_Lines, rule=Positive_Line_rule)
    model.Constraint_Negative_Line = pyo.Constraint(model.Set_Lines, rule=Negative_Line_rule)
    model.Objective_Function = pyo.Objective(rule=Objective_rule)
    
    results = pyo.SolverFactory('glpk').solve(model)
    
    return model, results

def ED_rule(model, x):
    return sum(model.Generation[xg] for xg in model.Set_Generators) == \
        sum(model.Loads[xl]['Value'] for xl in model.Set_Loads)

def get_ED(Generators, Loads):
    model = pyo.ConcreteModel()
    model.dual = pyo.Suffix(direction=pyo.Suffix.IMPORT_EXPORT)
    model.Generators = Generators
    model.Loads = Loads
    model.Lines = []
    get_Variables(model)
    model.Set_ED = range(1)
    model.Constraint_ED = pyo.Constraint(model.Set_ED, rule=ED_rule)
    model.Constraint_Min_Generation = pyo.Constraint(model.Set_Generators, rule=Min_Generation_rule)
    model.Constraint_Max_Generation = pyo.Constraint(model.Set_Generators, rule=Max_Generation_rule)
    model.Objective_Function = pyo.Objective(rule=Objective_rule)
    
    results = pyo.SolverFactory('glpk').solve(model)
    
    return model, results

def print_ED(model):
    print_OF(model)
    print_Generation(model)
    print('Nodal prices:')
    print('\tMC     = %7.2f'%(model.dual[model.Constraint_ED[0]]))

def Linearize_Gen(Generators, N, Gen_Range):
    Set_N = range(N)
    Gen = []
    ΔP = []
    for gen, rng in zip(Generators, Gen_Range):
        Bus = gen['Bus']
        pieces = numpy.linspace(rng[0], rng[1], N+1)
        flg = True
        for xp in Set_N:
            p = (pieces[xp]+pieces[xp+1])/2
            MC = gen['Cost'][1] + 2*gen['Cost'][2]*p
            if flg:
                Gen.append({'Bus':Bus, 'Min':0, 'Max':pieces[xp+1], 'MC':MC})
                flg = False
            else:
                Gen.append({'Bus':Bus, 'Min':0, 'Max':pieces[xp+1]-pieces[xp], 'MC':MC})
        ΔP.append(pieces[xp+1]-pieces[xp])
    return Gen, ΔP

def get_Gen_QP(model, N):
    Gen = []
    Set_Generators = range(int(len(model.Generators)/N))
    xp1 = 0
    for xg in Set_Generators:
        xp2 = xp1
        P = 0
        for xn in range(N):
            aux = model.Generation[xp2].value
            if aux > 0:
                P += aux
                xp2 += 1
            else:
                xp1 += N
                break
        Gen.append(P)
    return Gen

def print_Gen_QP(model, Generators, N):
    Gen = get_Gen_QP(model, N)
    Cost = 0
    for P, gen in zip(Gen, Generators):
        Cost += gen['Cost'][0] + gen['Cost'][1]*P + gen['Cost'][2]*P**2
    print('Cost = %7.2f £'%Cost)
    print('Generation outputs:')
    xg = 0
    for P in Gen:
        if P == Generators[xg]['Max']:
            txt = '(Binding constraint)'
        else:
            txt = ''
        xg += 1
        print('\tG%d     = %7.2f %s'%(xg, P, txt))
    return Gen

def print_QP(model, Generators, N):
    G = print_Gen_QP(model, Generators, N)
    PF = print_Power(model)
    NP = print_Nodal(model)
    MC = print_MCs(model)
    
    return G, PF, NP, MC

def get_QP_Direct(Generators, Lines, Loads, Loss=0, N=100):
    '''Piece-wise approximation: Direct method'''
    Gen_Range = [[gen['Min'], gen['Max']] for gen in Generators]
    Gen, _ = Linearize_Gen(Generators, N, Gen_Range)
    model, results = get_LP(Gen, Lines, Loads, Loss)
    _, _, _, _ = print_QP(model, Generators, N)

    return model, results

def get_QP_Iterative(Generators, Lines, Loads, Loss=0, N=10):
    '''Piece-wise approximation: Iterative method'''

    # First iteration, full linear approximation
    Gen_Range = [[gen['Min'], gen['Max']] for gen in Generators]
    
    Error = numpy.Inf
    Iteration = 0
    flg = True
    Iteration_Max = 50
    while flg:
    
        # Linearize
        Gen, ΔP = Linearize_Gen(Generators, N, Gen_Range)

        # Solve
        model, results = get_LP(Gen, Lines, Loads, Loss)

        # Get new estimations
        P = get_Gen_QP(model, N)

        # Reduce range
        Gen_Range = []
        Error = 0
        for p, Δp, gen in zip(P, ΔP, Generators):
            aux1 = max(gen['Min'], p-Δp*2)
            aux2 = min(gen['Max'], p+Δp*2)
            Error = max(Error, Δp)
            Gen_Range.append([aux1, aux2])

        if Error <= 0.001:
            flg = False
        else:
            if Iteration <= Iteration_Max:
                Iteration += 1
            else:
                flg = False
                print('The model failed to converge after %d iterations'%Iteration_Max)
    model, results = get_LP(Gen, Lines, Loads, Loss)
    G, PF, NP, MC = print_QP(model, Generators, N)
    print('Iterations:', Iteration)
    return model, results, G, NP

def get_ED_Iterative(Generators, Loads, N=10):
    '''Piece-wise approximation: Iterative method'''

    # First iteration, full linear approximation
    Gen_Range = [[gen['Min'], gen['Max']] for gen in Generators]
    
    Error = numpy.Inf
    Iteration = 0
    flg = True
    Iteration_Max = 50
    while flg:
    
        # Linearize
        Gen, ΔP = Linearize_Gen(Generators, N, Gen_Range)

        # Solve
        model, results = get_ED(Gen, Loads)

        P = get_Gen_QP(model, N)

        # Reduce range
        Gen_Range = []
        Error = 0
        for p, Δp, gen in zip(P, ΔP, Generators):
            aux1 = max(gen['Min'], p-Δp*2)
            aux2 = min(gen['Max'], p+Δp*2)
            Error = max(Error, Δp)
            Gen_Range.append([aux1, aux2])

        if Error <= 0.001:
            flg = False
        else:
            if Iteration <= Iteration_Max:
                Iteration += 1
            else:
                flg = False
                print('The model failed to converge after %d iterations'%Iteration_Max)
    model, results = get_ED(Gen, Loads)
    Gen = print_Gen_QP(model, Generators, N)
    
    print('Nodal prices:')
    MC = 0
    for G, gen in zip(Gen, Generators):
        MC = max(MC, gen['Cost'][1]+gen['Cost'][2]*2*G)
    print('\tMC     = %7.2f'%MC)
    print('Iterations:', Iteration)

    return model, results, Gen, MC